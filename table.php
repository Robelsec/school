<?php
include './connection.php';
include './header.php';
?>
<div class="col-md-12">
    <table class="table table-bordered" >

        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Short_Name</th>
            <th>Description</th>
            <th>Short_Description</th>

            <th>Established</th>
            <th>Logo</th>
            <th>Slug</th>
            <th>Background</th>
            <th>Edit</th>
            <th>Delete</th>



        </tr>
        <?php
        $sql2 = "select * from institution";
        $result = $conn->query($sql2);
        if ($result->num_rows > 0) {
            // output data of each row
            foreach ($result as $key => $row) {
                ?>
                <tr>

                    <td> <?php echo $row["ID"]; ?></td>

                    <td> <?php echo $row["Name"]; ?></td>
                    <td> <?php echo $row["Short_Name"]; ?></td>
                    <td> <?php echo $row["Description"]; ?></td>
                    <td> <?php echo $row["Short_Description"]; ?></td>
                    <td> <?php echo $row["Established"]; ?></td>
                    <td> <?php echo $row["Logo"]; ?></td>
                    <td> <?php echo $row["Slug"]; ?></td>

                    <td> <?php echo $row["Background"]; ?></td>

                    <td><a class="btn btn-xs btn-primary" href="edit2.php?id=<?php echo $row["ID"]; ?>">Edit</a></td>
                    <td><a class="btn btn-xs btn-danger" href="delete.php?id=<?php echo $row["ID"]; ?>" 
                           onclick="return confirm('Are you sure ? ')">Delete</a>
                        
                        <!--<a href="whatever" onclick="return confirm('are you sure?')">Re</a>-->
                        
                    
                    </td>
                           


                </tr>

                <?php
            }
        }
        ?>

    </table>
</div>
<?php
if (isset($_GET['success']) && $_GET['success'] == true) {
//    echo "<script>alert('".$_GET['msg']."');</script>";
        echo "<script>alert('Updated Successfully');</script>";

} elseif(isset($_GET['success']) && $_GET['success'] == false) {
    echo "<script>alert('Failed to Update');</script>";
}
?>

<?php include './footer.php'; ?>
